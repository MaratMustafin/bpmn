# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from collections import OrderedDict
from operator import itemgetter

from odoo import http, _
from odoo.exceptions import AccessError, MissingError
from odoo.http import request
from odoo.addons.portal.controllers.portal import CustomerPortal, pager as portal_pager
from odoo.tools import groupby as groupbyelemf

from odoo.osv.expression import OR


class CustomerPortal(CustomerPortal):

    def _prepare_home_portal_values(self):
        values = super(CustomerPortal, self)._prepare_home_portal_values()
        values['process_count'] = request.env['process.process'].search_count([])
        values['task_count'] = request.env['process.task'].search_count([])
        return values

    # ------------------------------------------------------------
    # My Process
    # ------------------------------------------------------------
    def _process_get_page_view_values(self, process, access_token, **kwargs):
        values = {
            'page_name': 'process',
            'process': process,
        }
        return self._get_page_view_values(process, access_token, values, 'my_processes_history', False, **kwargs)

    @http.route(['/my/processes', '/my/processes/page/<int:page>'], type='http', auth="user", website=True)
    def portal_my_processes(self, page=1, date_begin=None, date_end=None, sortby=None, **kw):
        values = self._prepare_portal_layout_values()
        Process = request.env['process.process']
        domain = []

        searchbar_sortings = {
            'date': {'label': _('Newest'), 'order': 'create_date desc'},
            'name': {'label': _('Name'), 'order': 'name'},
        }
        if not sortby:
            sortby = 'date'
        order = searchbar_sortings[sortby]['order']

        # archive groups - Default Group By 'create_date'
        archive_groups = self._get_archive_groups('process.process', domain) if values.get('my_details') else []
        if date_begin and date_end:
            domain += [('create_date', '>', date_begin), ('create_date', '<=', date_end)]
        # processes count
        process_count = Process.search_count(domain)
        # pager
        pager = portal_pager(
            url="/my/processes",
            url_args={'date_begin': date_begin, 'date_end': date_end, 'sortby': sortby},
            total=process_count,
            page=page,
            step=self._items_per_page
        )

        # content according to pager and archive selected
        processes = Process.search(domain, order=order, limit=self._items_per_page, offset=pager['offset'])
        request.session['my_processes_history'] = processes.ids[:100]

        values.update({
            'date': date_begin,
            'date_end': date_end,
            'processes': processes,
            'page_name': 'process',
            'archive_groups': archive_groups,
            'default_url': '/my/processes',
            'pager': pager,
            'searchbar_sortings': searchbar_sortings,
            'sortby': sortby
        })
        return request.render("process.portal_my_processes", values)

    @http.route(['/my/process/<int:process_id>'], type='http', auth="public", website=True)
    def portal_my_process(self, process_id=None, access_token=None, **kw):
        try:
            process_sudo = self._document_check_access('process.process', process_id, access_token)
        except (AccessError, MissingError):
            return request.redirect('/my')

        values = self._process_get_page_view_values(process_sudo, access_token, **kw)
        return request.render("process.portal_my_process", values)

    # ------------------------------------------------------------
    # My Task
    # ------------------------------------------------------------
    def _task_get_page_view_values(self, task, access_token, **kwargs):
        values = {
            'page_name': 'task',
            'task': task,
            'user': request.env.user
        }
        return self._get_page_view_values(task, access_token, values, 'my_tasks_history', False, **kwargs)

    @http.route(['/my/tasks', '/my/tasks/page/<int:page>'], type='http', auth="user", website=True)
    def portal_my_tasks(self, page=1, date_begin=None, date_end=None, sortby=None, filterby=None, search=None, search_in='content', groupby='process', **kw):
        values = self._prepare_portal_layout_values()
        searchbar_sortings = {
            'date': {'label': _('Newest'), 'order': 'create_date desc'},
            'name': {'label': _('Title'), 'order': 'name'},
            'stage': {'label': _('Stage'), 'order': 'stage_id'},
            'update': {'label': _('Last Stage Update'), 'order': 'date_last_stage_update desc'},
        }
        searchbar_filters = {
            'all': {'label': _('All'), 'domain': []},
        }
        searchbar_inputs = {
            'content': {'input': 'content', 'label': _('Search <span class="nolabel"> (in Content)</span>')},
            'message': {'input': 'message', 'label': _('Search in Messages')},
            'customer': {'input': 'customer', 'label': _('Search in Customer')},
            'stage': {'input': 'stage', 'label': _('Search in Stages')},
            'all': {'input': 'all', 'label': _('Search in All')},
        }
        searchbar_groupby = {
            'none': {'input': 'none', 'label': _('None')},
            'process': {'input': 'process', 'label': _('Process')},
        }

        # extends filterby criteria with process the customer has access to
        processes = request.env['process.process'].search([])
        for process in processes:
            searchbar_filters.update({
                str(process.id): {'label': process.name, 'domain': [('process_id', '=', process.id)]}
            })

        # extends filterby criteria with process (criteria name is the process id)
        # Note: portal users can't view processes they don't follow
        process_groups = request.env['process.task'].read_group([('process_id', 'not in', processes.ids)],
                                                                ['process_id'], ['process_id'])
        for group in process_groups:
            proj_id = group['process_id'][0] if group['process_id'] else False
            proj_name = group['process_id'][1] if group['process_id'] else _('Others')
            searchbar_filters.update({
                str(proj_id): {'label': proj_name, 'domain': [('process_id', '=', proj_id)]}
            })

        # default sort by value
        if not sortby:
            sortby = 'date'
        order = searchbar_sortings[sortby]['order']
        # default filter by value
        if not filterby:
            filterby = 'all'
        domain = searchbar_filters.get(filterby, searchbar_filters.get('all'))['domain']

        # archive groups - Default Group By 'create_date'
        archive_groups = self._get_archive_groups('process.task', domain) if values.get('my_details') else []
        if date_begin and date_end:
            domain += [('create_date', '>', date_begin), ('create_date', '<=', date_end)]

        # search
        if search and search_in:
            search_domain = []
            if search_in in ('content', 'all'):
                search_domain = OR([search_domain, ['|', ('name', 'ilike', search), ('description', 'ilike', search)]])
            if search_in in ('customer', 'all'):
                search_domain = OR([search_domain, [('partner_id', 'ilike', search)]])
            if search_in in ('message', 'all'):
                search_domain = OR([search_domain, [('message_ids.body', 'ilike', search)]])
            if search_in in ('stage', 'all'):
                search_domain = OR([search_domain, [('stage_id', 'ilike', search)]])
            domain += search_domain

        # task count
        task_count = request.env['process.task'].search_count(domain)
        # pager
        pager = portal_pager(
            url="/my/tasks",
            url_args={'date_begin': date_begin, 'date_end': date_end, 'sortby': sortby, 'filterby': filterby, 'search_in': search_in, 'search': search},
            total=task_count,
            page=page,
            step=self._items_per_page
        )
        # content according to pager and archive selected
        if groupby == 'process':
            order = "process_id, %s" % order  # force sort on process first to group by process in view
        tasks = request.env['process.task'].search(domain, order=order, limit=self._items_per_page, offset=(page - 1) * self._items_per_page)
        request.session['my_tasks_history'] = tasks.ids[:100]
        if groupby == 'process':
            grouped_tasks = [request.env['process.task'].concat(*g) for k, g in groupbyelem(tasks, itemgetter('process_id'))]
        else:
            grouped_tasks = [tasks]

        values.update({
            'date': date_begin,
            'date_end': date_end,
            'grouped_tasks': grouped_tasks,
            'page_name': 'task',
            'archive_groups': archive_groups,
            'default_url': '/my/tasks',
            'pager': pager,
            'searchbar_sortings': searchbar_sortings,
            'searchbar_groupby': searchbar_groupby,
            'searchbar_inputs': searchbar_inputs,
            'search_in': search_in,
            'sortby': sortby,
            'groupby': groupby,
            'searchbar_filters': OrderedDict(sorted(searchbar_filters.items())),
            'filterby': filterby,
        })
        return request.render("process.portal_my_tasks", values)

    @http.route(['/my/task/<int:task_id>'], type='http', auth="public", website=True)
    def portal_my_task(self, task_id, access_token=None, **kw):
        try:
            task_sudo = self._document_check_access('process.task', task_id, access_token)
        except (AccessError, MissingError):
            return request.redirect('/my')

        # ensure attachment are accessible with access token inside template
        for attachment in task_sudo.attachment_ids:
            attachment.generate_access_token()
        values = self._task_get_page_view_values(task_sudo, access_token, **kw)
        return request.render("process.portal_my_task", values)
