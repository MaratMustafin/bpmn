odoo.define('markup2html_widget', function (require) {
"use strict";


    var AbstractField = require('web.AbstractField');
    var FieldRegistry = require('web.field_registry');
    var field_utils = require('web.field_utils');

    var core = require('web.core');
    var qweb = core.qweb;

    var markup2html = AbstractField.extend({
        step: 1, 
        template: 'markup2htmlTemplate', 
        init: function () {
            this._super.apply(this, arguments);
        },
        _render: function () {

            var self = this;
            console.log(self);
        },

    });

    FieldRegistry.add('markup2html', markup2html);

    return markup2html;

});

