# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models


class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    module_process_forecast = fields.Boolean(string="Forecasts")
    module_hr_timesheet = fields.Boolean(string="Task Logs")
    group_subtask_process = fields.Boolean("Sub-tasks", implied_group="bpmn.group_subtask_process")
    group_process_rating = fields.Boolean("Use Rating on Process", implied_group='bpmn.group_process_rating')
