# Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

import difflib

from odoo import _, api, fields, models


class ProcessHistory(models.Model):
    """This model is necessary to manage a process history."""

    _name = "process.history"
    _description = "Process History"
    _order = "id DESC"

    page_id = fields.Many2one("process.process", "Process", ondelete="cascade")
    proc_name = fields.Text(string="Process Name")
    name = fields.Char(index=True, string="Change")
    summary = fields.Char(index=True, string="Change Summary")
    content = fields.Text(string="Description")
    goal = fields.Char(string="Process Goal", readonly=False)
    strategic_kpi = fields.Char(string="Strategic KPI", readonly=False)
    parent = fields.Text(string="Parent Process")
    children = fields.Text(string="Children Processes")
    diff = fields.Text(string="Version Difference", compute="_compute_diff")
    in_docs = fields.Text(string="In Documents")
    out_docs = fields.Text(string="Out Documents")
    add_docs = fields.Text(string="Additional Documents")
    # in_docs = fields.Many2many(comodel_name="document.page",
    #                                    string="In documents")
    # out_docs = fields.Many2many(comodel_name="document.page",
    #                                     string="Out documents")
    # add_docs = fields.Many2many(comodel_name="document.page",
    #                                     string="Additional documents")

    company_id = fields.Many2one(
        "res.company",
        "Company",
        help="If set, page is accessible only from this company",
        related="page_id.company_id",
        store=True,
        index=True,
        readonly=True,
    )

    def _compute_diff(self):
        """Shows a diff between this version and the previous version"""
        history = self.env["process.history"]
        for rec in self:
            prev = history.search(
                [
                    ("page_id", "=", rec.page_id.id),
                    ("create_date", "<", rec.create_date),
                ],
                limit=1,
                order="create_date DESC",
            )
            self._compare_fields(prev.id,rec.id)
            print(prev.content,rec.content)
            print(prev.goal,rec.goal)
            self._get_diff_goal(prev.id,rec.id)
            rec.diff = self._get_diff(prev.id, rec.id)
            print(rec.diff)

    @api.model
    def _get_diff(self, v1, v2):
        """Return the difference between two version of process version."""
        text1 = v1 and self.browse(v1).content or ""
        text2 = v2 and self.browse(v2).content or ""
        print("Я здесь1",text1)
        print("Я здесь2",text2)

        # Include line breaks to make it more readable
        # TODO: consider using a beautify library directly on the content
        text1 = text1.replace("</p><p>", "</p>\r\n<p>")
        text2 = text2.replace("</p><p>", "</p>\r\n<p>")
        line1 = text1.splitlines(True)
        line2 = text2.splitlines(True)
        if line1 == line2:
            return _("Никаких изменений не найдено.")
        else:
            diff = difflib.HtmlDiff()
            return diff.make_table(
                line1,
                line2,
                "Revision-{}".format(v1),
                "Revision-{}".format(v2),
                context=True,
            )

    def name_get(self):
        return [(rec.id, "%s #%i" % (rec.page_id.name, rec.id)) for rec in self]


    @api.model
    def _get_diff_goal(self,v1,v2):
        """Return the difference between two version of process version."""
        text1 = v1 and self.browse(v1).goal or ""
        text2 = v2 and self.browse(v2).goal or ""
        print("Я здесь1",text1)
        print("Я здесь2",text2)
        line1 = text1.splitlines(True)
        line2 = text2.splitlines(True)
        if line1 == line2:
            print("Никаких изменений не найдено для целей")
        else:
            diff = difflib.HtmlDiff()
            print(diff.make_table(
                line1,
                line2,
                "Изменения - {}".format(v1),
                "Изменения - {}".format(v2),
                context=True
            ))
        # # Include line breaks to make it more readable
        # # TODO: consider using a beautify library directly on the content
        # text1 = text1.replace("</p><p>", "</p>\r\n<p>")
        # text2 = text2.replace("</p><p>", "</p>\r\n<p>")
        # line1 = text1.splitlines(True)
        # line2 = text2.splitlines(True)
        # if line1 == line2:
        #     return _("Никаких изменений не найдено.")
        # else:
        #     diff = difflib.HtmlDiff()
        #     return diff.make_table(
        #         line1,
        #         line2,
        #         "Revision-{}".format(v1),
        #         "Revision-{}".format(v2),
        #         context=True,
        #     )        
    @api.model
    def _compare_fields(self, v1, v2):

        proc_name1 = v1 and self.browse(v1).proc_name or ""
        name1 = v1 and self.browse(v1).name or ""
        summary1 = v1 and self.browse(v1).summary or ""
        content1 = v1 and self.browse(v1).content or ""
        goal1 = v1 and self.browse(v1).goal or ""
        strategic_kpi1 = v1 and self.browse(v1).strategic_kpi or ""
        parent1 = v1 and self.browse(v1).parent or ""
        children1 = v1 and self.browse(v1).children or ""
        in_docs1 = v1 and self.browse(v1).in_docs or ""
        out_docs1 = v1 and self.browse(v1).out_docs or ""
        add_docs1 = v1 and self.browse(v1).add_docs or ""
        print("""Type proc_name - {}, name - {}, summary - {},content - {},
            goal - {}, strategic_kpi - {}, parent - {}, children - {}
            , in_docs - {}, out_docs - {}, add_docs - {}""".format(proc_name1,name1,
            summary1,content1,goal1,strategic_kpi1,parent1,children1,in_docs1,out_docs1,
            add_docs1))
        diff = list()
        diff.append(str(int(self.browse(v1).proc_name == self.browse(v2).proc_name)))
        diff.append(str(int(self.browse(v1).name == self.browse(v2).name)))
        diff.append(str(int(self.browse(v1).summary == self.browse(v2).summary)))
        diff.append(str(int(self.browse(v1).content == self.browse(v2).content)))
        diff.append(str(int(self.browse(v1).goal == self.browse(v2).goal)))
        diff.append(str(int(self.browse(v1).strategic_kpi == self.browse(v2).strategic_kpi)))
        diff.append(str(int(self.browse(v1).parent == self.browse(v2).parent)))
        diff.append(str(int(self.browse(v1).children == self.browse(v2).children)))
        diff.append(str(int(self.browse(v1).in_docs == self.browse(v2).in_docs)))
        diff.append(str(int(self.browse(v1).out_docs == self.browse(v2).out_docs)))
        diff.append(str(int(self.browse(v1).add_docs == self.browse(v2).add_docs)))
        # print("IM HERE",diff)
        diff2 = list()
        print("".join(diff))
