# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from datetime import timedelta

from odoo import api, fields, models, tools, SUPERUSER_ID, _
from odoo.exceptions import UserError, AccessError, ValidationError
from odoo.tools.safe_eval import safe_eval
from odoo.tools.misc import format_date
import pdb
# from functools import lru_cache
import pprint
import re

import base64
import graphviz as gv

class ProcessRole(models.Model):
    _name = 'process.role'
    _description = ''


    name = fields.Char(string='Ответственный за поставку')
    process_id = fields.Many2one('process.process')

class ProcessTaskType(models.Model):
    _name = 'process.task.type'
    _description = 'Task Stage'
    _order = 'sequence, id'

    def _get_default_process_ids(self):
        default_process_id = self.env.context.get('default_process_id')
        return [default_process_id] if default_process_id else None

    name = fields.Char(string='Stage Name', required=True, translate=True)
    description = fields.Text(translate=True)
    sequence = fields.Integer(default=1)
    process_ids = fields.Many2many('process.process', 'process_task_type_rel', 'type_id', 'process_id',
                                   string='processes',
                                   default=_get_default_process_ids)
    legend_blocked = fields.Char(
        'Red Kanban Label', default=lambda s: _('Blocked'), translate=True, required=True,
        help='Override the default value displayed for the blocked state for kanban selection, when the task or issue is in that stage.')
    legend_done = fields.Char(
        'Green Kanban Label', default=lambda s: _('Ready for Next Stage'), translate=True, required=True,
        help='Override the default value displayed for the done state for kanban selection, when the task or issue is in that stage.')
    legend_normal = fields.Char(
        'Grey Kanban Label', default=lambda s: _('In Progress'), translate=True, required=True,
        help='Override the default value displayed for the normal state for kanban selection, when the task or issue is in that stage.')
    mail_template_id = fields.Many2one(
        'mail.template',
        string='Email Template',
        domain=[('model', '=', 'process.task')],
        help="If set an email will be sent to the customer when the task or issue reaches this step.")
    fold = fields.Boolean(string='Folded in Kanban',
                          help='This stage is folded in the kanban view when there are no records in that stage to display.')
    rating_template_id = fields.Many2one(
        'mail.template',
        string='Rating Email Template',
        domain=[('model', '=', 'process.task')],
        help="If set and if the process's rating configuration is 'Rating when changing stage', then an email will be sent to the customer when the task reaches this step.")
    auto_validation_kanban_state = fields.Boolean('Automatic kanban status', default=False,
                                                  help="Automatically modify the kanban state when the customer replies to the feedback for this stage.\n"
                                                       " * A good feedback from the customer will update the kanban state to 'ready for the new stage' (green bullet).\n"
                                                       " * A medium or a bad feedback will set the kanban state to 'blocked' (red bullet).\n")

    def unlink(self):
        stages = self
        default_process_id = self.env.context.get('default_process_id')
        if default_process_id:
            shared_stages = self.filtered(lambda x: len(
                x.process_ids) > 1 and default_process_id in x.process_ids.ids)
            tasks = self.env['process.task'].with_context(active_test=False).search(
                [('process_id', '=', default_process_id), ('stage_id', 'in', self.ids)])
            if shared_stages and not tasks:
                shared_stages.write({'process_ids': [(3, default_process_id)]})
                stages = self.filtered(lambda x: x not in shared_stages)
        return super(ProcessTaskType, stages).unlink()


class ProcessLine(models.Model):
    _name = 'process.process.line'
    _description = ''

    consumer_id = fields.Many2one('process.process')
    provider_id = fields.Many2one('process.process')


class ProcessLineDocument(models.Model):
    _name = 'process.process.line.document'
    _description = ''

    in_process_id = fields.Many2one('process.process')
    out_process_id = fields.Many2one('process.process')

    second_out_process_id = fields.Many2one('process.process')

    add_process_id = fields.Many2one('process.process')
    second_out_process_id = fields.Many2one('process.process')

    stage = fields.Selection(
        [
            ('draft', 'Черновик'),
            ('signed', 'Подписан')
        ], string='Stage of Documents', default='draft'
    )
    future_stage = fields.Selection(
        [
            ('draft', 'Draft'),
            ('signed', 'Signed')
        ], string='Future Stage of Documents'
    )

    main_document = fields.Boolean(string='Chief document')
    document = fields.Many2one('document.page')


class Process(models.Model):
    _name = "process.process"
    _description = "process"
    _inherit = ['portal.mixin', 'mail.alias.mixin',
                'mail.thread', 'rating.parent.mixin']
    _order = "sequence, name, id"
    _period_number = 5


    role_ids = fields.One2many('process.role','process_id')
    #type_of_event = fields.Selection(selection='_function',string='Типы процессов')
    type_of_event = fields.Reference(
        selection='_type_of_event', string='Типы Ивентов')
    
    test_image = fields.Image(string='Test Image')
    # selection_of_type = fields.Selection([('default','Default')],string='Ивент')

    def _type_of_event(self):
        ''' 
            1. Event
            2. Document
            3. Another Process
        '''
        process_models = self.env['ir.model'].search(
            [('model', '=', 'process.event')])
        document_models = self.env['ir.model'].search(
            [('model', '=', 'document.page')])
        event_models = self.env['ir.model'].search(
            [('model', '=', 'process.event')])
        # print('Процессы:',process_models)
        # print('*'*80)
        # print('Документы:',document_models)
        # print('*'*80)
        # print('Ивенты:',event_models)

        return [(process_models.model, process_models.name)]
        # return [('process.event','Ивенты'),('document.page','Документы'),('process.process','Процессы')]

    '''
    @api.onchange('type_of_event')
    def _change_type_of_event(self):
        if self.type_of_event == 'process.process':
            process_models = self.env['process.process'].search([])
            self.selection_of_type = [(model) for model in process_models]
        if self.type_of_event == 'document.page':
            document_models = self.env['document.page'].search([])
            #name = [(model.name) for model in document_models]
            self.selection_of_type = [(model) for model in document_models]
        if self.type_of_event == 'process.event':
            event_models = self.env['process.event'].search([])
            #name = [(model.name) for model in event_models]
            self.selection_of_type = [(model) for model in event_models]
    '''

    # ============Custom Attributes============
    developed_by = fields.Many2one('hr.employee', string="Developed by")
    approved_by = fields.Many2one('hr.employee', string="Approved by")
    is_final = fields.Boolean(compute="compare_depth", string="", store=True)
    max_depth = fields.Integer(
        compute="find_max_depth", string="Maximum Possible Depth", store=True)
    n_of_children = fields.Integer(
        compute='_count_children', string='Child Processes', store=True)
    depth = fields.Integer(compute="_calculate_depth",
                           string="Depth of the Current Process", store=True)
    n_of_working = fields.Integer(default=0, string="Number of Working")
    n_of_owners = fields.Integer(
        compute='_count_owners', string="Number of Owners", store=True)
    kpi = fields.Html("KPI", required=False)
    strategic_kpi = fields.Char(
        "Strategic KPI", required=False, related="history_head.strategic_kpi", readonly=False)

    main_process_provider = fields.Many2one(
        "process.process",
        string="Основной поставщик процесса"
    )

    child_process_provider_ids = fields.One2many(
        comodel_name='process.process',
        inverse_name='main_process_provider',
        string='Основной вход процесса'
    )

    process_lines_ids = fields.One2many(
        comodel_name='process.process.line', inverse_name='consumer_id', string='Main Process Provider')
    in_process_lines_document_ids = fields.One2many(
        comodel_name='process.process.line.document', inverse_name='in_process_id', string='In Documents')
    out_process_lines_document_ids = fields.One2many(
        comodel_name='process.process.line.document', inverse_name='out_process_id', string='Out Documents')
    add_process_lines_document_ids = fields.One2many(
        comodel_name='process.process.line.document', inverse_name='add_process_id', string='Add Documents')
    # ============Custom Attributes============

    vhod_process = fields.Html(
        string='The Main Input of The Process ', compute='_move_process_lines_documents_to')

    def _move_process_lines_documents_to(self):
        self.vhod_process = False
        for record in self:
            variable = {
                "record_in_process": record.in_process_lines_document_ids}
            pprint.pprint(variable)
            # if len(record.process_lines_ids.provider_id) >= 0:
            #     all_records = record.in_process_lines_document_ids
            #     for i in all_records:
            #         i.unlink()
            #         self.vhod_process = False
            if len(record.process_lines_ids.provider_id) >= 1:
                all_in_provider = []
                for provider_id in record.process_lines_ids.provider_id:
                    all_in_provider.extend(provider_id)
                print(all_in_provider)
                for out in record.process_lines_ids.provider_id.out_process_lines_document_ids:
                    for provider in all_in_provider:
                        if provider == out.out_process_id:
                            instance = self.env['process.process.line.document'].search(
                                [('in_process_id', '=', record.id), ('out_process_id', '=', provider.id), ('document', '=', out.document.id)])
                            instance2 = self.env['process.process.line.document'].search(
                                [('in_process_id','=',record.id),('second_out_process_id','=',provider.id),('document','=',out.document.id)]
                            )
                            if instance:
                                text = {'record': record.id, 'out_process_id': provider.id, 'stage': out.stage,
                                        'main_document': out.main_document, 'document': out.document.id}
                                pprint.pprint(text)
                                self.compute_vhod_process()
                            elif instance2:
                                self.compute_vhod_process()
                            else:
                                new_process_line_document = self.env['process.process.line.document'].create({
                                    'in_process_id': record.id,
                                    'second_out_process_id':provider.id,
                                    'stage': out.stage,
                                    'main_document': out.main_document,
                                    'document': out.document.id
                                })
                                self.compute_vhod_process()

    def _set_documents_to(self):
        for rec in self:
            print("From Inverse:", rec)

    @api.depends('child_process_provider_ids.out_document_ids')
    def _move_documents_to(self):
        self.in_document_ids = False
        all_docs = []
        for rec in self:
            for provider in rec.child_process_provider_ids:
                all_docs.extend(provider.out_document_ids.ids)
            rec.in_document_ids = all_docs

    _check_company_auto = True

    _HTML_WIDGET_DEFAULT_VALUE = "<p><br></p>"

    def onchange_number(self):
        return self.number + '.' + str(self.n_of_children + 1)

    @api.constrains('max_depth')
    def check_depth(self):
        if self.max_depth < 1:
            raise ValidationError("Process depth should be at least 1!")

    @api.depends('max_depth', 'depth')
    def compare_depth(self):
        for rec in self:
            if rec.max_depth == rec.depth:
                rec.is_final = True
            else:
                rec.is_final = False

    @api.depends('max_depth', 'depth')
    def find_max_depth(self):
        for rec in self:
            if rec.parent_id:
                rec.max_depth = rec.parent_id.max_depth

    def increase_depth(self):
        self.max_depth += 1
        self.compare_depth()

    def open_doc(self):
        return {
            'name': _('Name of the form'),
            'view_mode': 'form',
            'view_id': False,
            'views': 'form',
            'view_type': 'form',
            'res_id': self.id,  # id of the object to which to redirected
            'res_model': 'document.page',
            'type': 'ir.actions.act_window',
            'target': 'new',  # open the form in new tab
        }

    @api.depends('child_ids')
    def open_action(self):
        self.parent_id.child_ids2 = [(5, 0, 0)]
        self.parent_id.child_ids3 = [(5, 0, 0)]
        ctx = dict(self.env.context)
        ctx['default_number'] = self.env.context.get('number', self.number) + '.' + str(
            len(self.env.context.get('child_ids', self.child_ids)) + 1)
        ctx['search_default_number'] = self.number
        return {
            'name': 'Popup',
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': self._name,
            'res_id': self.id,
            'target': 'current',
            'context': ctx
        }

    @api.depends('child_ids')
    def clear_children(self):
        self.child_ids2 = [(5, 0, 0)]
        self.child_ids3 = [(5, 0, 0)]

    @api.depends('child_ids')
    def list_children(self):
        temp = list()
        for x in self.child_ids:
            temp.append(x.id)
        self.parent_id.child_ids2 = [(6, 0, temp)]
        self.parent_id.child_ids3 = [(5, 0, 0)]

    @api.depends('child_ids')
    def list_children2(self):
        temp = list()
        for x in self.child_ids:
            temp.append(x.id)
        self.parent_id.parent_id.child_ids3 = [(6, 0, temp)]

    @api.depends('child_ids')
    def action_subprocess(self):
        action = self.env.ref('process.open_view_process_all').read()[0]

        # only display subtasks of current task
        action['domain'] = [('id', 'child_of', self.id), ('id', '!=', self.id)]
        # print(action)
        ctx = dict(self.env.context)
        print(ctx)
        ctx.update({
            'default_number': self.env.context.get('number', self.number) + '.' + str(
                len(self.env.context.get('child_ids', self.child_ids)) + 1),
            'default_parent_id': self.id,  # will give default subtask field in `default_get`
            'search_default_parent_id': self.id,
        })
        print(ctx)
        action['context'] = ctx
        # print(action)
        return action

    @api.depends("parent_id", "parent_id.depth")
    def _calculate_depth(self):
        for rec in self:
            if rec.parent_id:
                rec.depth = rec.parent_id.depth + 1
            else:
                rec.depth = 1

    @api.depends('child_ids')
    def _count_owners(self):
        for rec in self:
            rec.n_of_owners = 1

    @api.depends('child_ids')
    def _count_children(self):
        for rec in self:
            rec.n_of_children = len(rec.child_ids)

    def get_alias_model_name(self, vals):
        return vals.get('alias_model', 'process.task')

    def get_alias_values(self):
        values = super(Process, self).get_alias_values()
        values['alias_defaults'] = {'process_id': self.id}
        return values

    def _compute_attached_docs_count(self):
        Attachment = self.env['ir.attachment']
        for process in self:
            process.doc_count = Attachment.search_count([
                '|',
                '&',
                ('res_model', '=', 'process.process'), ('res_id', '=', process.id),
                '&',
                ('res_model', '=', 'process.task'), ('res_id',
                                                     'in', process.task_ids.ids)
            ])

    def _compute_task_count(self):
        task_data = self.env['process.task'].read_group(
            [('process_id', 'in', self.ids), '|',
             ('stage_id.fold', '=', False), ('stage_id', '=', False)],
            ['process_id'], ['process_id'])
        result = dict((data['process_id'][0], data['process_id_count'])
                      for data in task_data)
        for process in self:
            process.task_count = result.get(process.id, 0)

    def attachment_tree_view(self):
        attachment_action = self.env.ref('base.action_attachment')
        action = attachment_action.read()[0]
        action['domain'] = str([
            '|',
            '&',
            ('res_model', '=', 'process.process'),
            ('res_id', 'in', self.ids),
            '&',
            ('res_model', '=', 'process.task'),
            ('res_id', 'in', self.task_ids.ids)
        ])
        action['context'] = "{'default_res_model': '%s','default_res_id': %d}" % (
            self._name, self.id)
        return action

    @api.model
    def activate_sample_process(self):
        """ Unarchives the sample process 'process.process_process_data' and
            reloads the process dashboard """
        # Unarchive sample process
        process = self.env.ref('process.process_process_data', False)
        if process:
            process.write({'active': True})

        cover_image = self.env.ref('process.msg_task_data_14_attach', False)
        cover_task = self.env.ref('process.process_task_data_14', False)
        if cover_image and cover_task:
            cover_task.write({'displayed_image_id': cover_image.id})

        # Change the help message on the action (no more activate process)
        action = self.env.ref('process.open_view_process_all', False)
        action_data = None
        if action:
            action.sudo().write({
                "help": _('''<p class="o_view_nocontent_smiling_face">
                    Create a new process</p>''')
            })
            action_data = action.read()[0]
        # Reload the dashboard
        return action_data

    def _compute_is_favorite(self):
        for process in self:
            process.is_favorite = self.env.user in process.favorite_user_ids

    def _inverse_is_favorite(self):
        favorite_processes = not_fav_processes = self.env['process.process'].sudo(
        )
        for process in self:
            if self.env.user in process.favorite_user_ids:
                favorite_processes |= process
            else:
                not_fav_processes |= process

        # Process User has no write access for process.
        not_fav_processes.write({'favorite_user_ids': [(4, self.env.uid)]})
        favorite_processes.write({'favorite_user_ids': [(3, self.env.uid)]})

    def _get_default_favorite_user_ids(self):
        return [(6, 0, [self.env.uid])]

    @api.onchange('owner_id')
    def change_job_position(self):
        for rec in self:
            # res_users = self.env['res.users'].search(['id','=',rec.])
            print("Я здесь:", rec.owner_id)
            return True

    number = fields.Char(string="Number", required=True,
                         tracking=True, store=True)
    name = fields.Char("Name", index=True, required=True, tracking=True)

    goal = fields.Char("Goal", required=False,
                       related="history_head.goal", readonly=False)
    owner_id = fields.Many2one(
        'res.users', string='Process Owner', default=lambda self: self.env.user)

    count_in_document = fields.Char(
        string='Count InDocuments', compute='_count_in_document')
    count_out_document = fields.Char(
        string='Count OutDocuments', compute='_count_out_document')
    # stage_documents = fields.Html(string='Stage Documents',compute='_compute_stage_documents')
    name_of_main_process_provider = fields.Html(
        string='Основной поставщик процесса', compute='_compute_name_of_main_process_provider', readonly=True)
    main_process_provider_event_start = fields.Html(
        string='Main Process Event Start', compute='_compute_event_start_of_main_process_provider', readonly=True)
    main_process_provider_event_end = fields.Html(
        stirng='Main Process Event End', compute='_compute_event_end_of_main_process_provider', readonly=True)

    def _compute_event_end_of_main_process_provider(self):
        self.main_process_provider_event_end = False
        text = '<strong>Main docs:</strong><br /><div style="width:350px; border: 1px solid silver; padding: 5px;">'
        for rec in self:
            for doc in rec.out_process_lines_document_ids:
                if doc.main_document == True:
                    if doc.stage == 'draft' or doc.stage == False:
                        doc.stage = 'signed'
                        text += doc.document.name + ' <strong>Подписан</strong><br />'
                    else:
                        text += doc.document.name + ' <strong>Подписан</strong><br />'
                # else:
                #     text = '<div style="width:350px">Основных документов нет'
            text += '</div>'
            rec.main_process_provider_event_end = text

    def _compute_event_start_of_main_process_provider(self):
        self.main_process_provider_event_start = False
        text = '<strong>Main docs:</strong><br /><div style="width:350px; border: 1px solid silver; padding: 5px;">'
        for rec in self:
            for doc in rec.process_lines_ids.provider_id.out_process_lines_document_ids:
                # text = {'rec_id':rec.id,'doc.in_process_id':doc.in_process_id.id,'type_of_doc':type(doc.document.name)}
                # pprint.pprint(text)
                if rec.id != doc.in_process_id.id:
                    print('DOC.MAIN_DOCUMENT:', doc.main_document)
                    if doc.main_document == True:
                        text += 'Document name:<strong>' + doc.document.name + '</strong>. Stage - <strong>' + doc.stage + '</strong><br />'
                    # else:
                    #     text = '<div style="width:350px">Основных документов нет'
            text += '</div>'
            rec.main_process_provider_event_start = text

    def _compute_name_of_main_process_provider(self):
        self.name_of_main_process_provider = False
        text = ''
        for rec in self:
            for child in rec.child_process_provider_ids:
                text += 'БП ' + '<strong>' + \
                    str(child.number) + '</strong>' + \
                    ' ' + str(child.name) + '<br />'
            rec.name_of_main_process_provider = text

    # @lru_cache
    # def count_main_out_document(self):
    #     for rec in self:
    #         a = [i.main_document for i in rec.out_document_ids]
    #     return a

    # @lru_cache
    # def count_main_add_document(self):
    #     for rec in self:
    #         a = [i.main_document for i in rec.out_document_ids]
    #     return a

    def output_kanban(self, label, html, doc_instance):
        if len(doc_instance) == 0:
            html = label + ' docs empty <br />'
            #html += '<span style="color: gray; font-size:12px;">Всего доп.документов:' + '<strong>'+str(len(doc_instance))+'</strong></span>'
            return html
        score = 0
        for instance in doc_instance:
            if self.id != instance.in_process_id.id:
                if instance.main_document == False:
                    score += 1
                    if instance.stage == 'draft':
                        html += 'Document name:<srong>' + instance.document.name + '</strong>. Stage - ' + \
                            '<span style=color:gray>' + str(instance.stage) + '</span>' + '<br />'
                    else:
                        html += instance.document.name + '/' + '<span style=color:green>' + \
                            str(instance.stage) + '</span>' + '<br />'
                # else:
                #     html = label + ' документы отсутствуют <br />'
        html += '<span style="color: gray; font-size:12px;">Sum of additional docs:' + \
            str(score) + '</span>'
        return html

    # @api.depends('stage_documents')

    def compute_vhod_process(self):
        # self.stage_documents = False
        for rec in self:
            HTML = '<div style="width:350px"><strong>Additional docs:</strong> <br /> <div style="border: 1px solid silver; padding: 5px;">'
            HTML += self.output_kanban('Additional', '',
                                       rec.process_lines_ids.provider_id.out_process_lines_document_ids)
            HTML += '</div>'
            # HTML += '<strong>Доп.документы:</strong> <br /> <div style="border: 1px solid silver; padding: 5px;">'
            # HTML += self.output_kanban('Дополнительные','',rec.process_lines_ids.provider_id.add_document_ids)
            # HTML += '</div>'
            # TEXT = '<span style="color: gray; font-size:12px;">Общее кол-во документов:' + '<strong>'+str(len(rec.process_lines_ids.provider_id.out_process_lines_document_ids) + len(rec.process_lines_ids.provider_id.add_process_lines_document_ids))+'</strong></span>'
            # HTML += TEXT + '</div>'
            rec.vhod_process = HTML

    def find_numbers(self, number):
        def func1(a): return (a % 100)//10 != 1 and a % 10 == 1
        def func2(a): return (a % 100)//10 != 1 and a % 10 in [2, 3, 4]
        return " документ" if func1(number) else " документа" if func2(number) else " документов"

    @api.depends('in_document_ids')
    def _count_in_document(self):
        for rec in self:
            numbers = str(len(rec.in_document_ids)) + \
                self.find_numbers(len(rec.in_document_ids))
            rec.count_in_document = numbers

    @api.depends('out_document_ids')
    def _count_out_document(self):
        for rec in self:
            numbers = str(len(rec.out_document_ids)) + \
                self.find_numbers(len(rec.out_document_ids))
            rec.count_out_document = numbers

    #stage_of_in_document = fields.Char(related='document.page.stage',string='Stage of In Document')

    in_document_ids = fields.Many2many(comodel_name="document.page", relation="document_page_process_in_docs_rel",
                                       string="In documents")

    # добавить теги состояний для документов

    out_document_ids = fields.Many2many(comodel_name="document.page", relation="document_page_process_out_docs_rel",
                                        string="Out documents")
    add_document_ids = fields.Many2many(comodel_name="document.page", relation="document_page_process_add_docs_rel",
                                        string="Additional documents")
    parent_id = fields.Many2one(
        comodel_name="process.process", string="Parent")
    parent_id2 = fields.Many2one(
        comodel_name="process.process", string="Parent")
    parent_id3 = fields.Many2one(
        comodel_name="process.process", string="Parent")
    child_ids = fields.One2many(
        comodel_name="process.process",
        inverse_name="parent_id",
        string="Children"
    )
    child_ids2 = fields.One2many(
        comodel_name="process.process",
        inverse_name="parent_id2",
        string="Level 2",
    )
    child_ids3 = fields.One2many(
        comodel_name="process.process",
        inverse_name="parent_id3",
        string="Level 3",
    )
    content = fields.Char(
        string="Description"
    )

    draft_name = fields.Char(
        string="Name",
        help="Name for the changes made",
        readonly=False,
        compute="_compute_name",
        inverse="_inverse_name",
        search="_search_name",
        store=False,
    )

    draft_summary = fields.Char(
        string="Summary",
        help="Describe the changes made",
        related="history_head.summary",
        readonly=False,
    )

    template = fields.Html(
        "Template",
        help="Template that will be used as a content template "
             "for all new page of this category.",
    )
    history_head = fields.Many2one(
        "process.history",
        "HEAD",
        compute="_compute_history_head",
        store=True,
        auto_join=True,
    )

    history_ids = fields.One2many(
        "process.history",
        "page_id",
        "History",
        order="create_date DESC",
        readonly=True,
    )
    menu_id = fields.Many2one("ir.ui.menu", "Menu", readonly=True)
    content_date = fields.Datetime(
        "Last Contribution Date",
        related="history_head.create_date",
        store=True,
        index=True,
        readonly=True,
    )
    content_uid = fields.Many2one(
        "res.users",
        "Last Contributor",
        related="history_head.create_uid",
        store=True,
        index=True,
        readonly=True,
    )
    backend_url = fields.Char(
        string="Backend URL",
        help="Use it to link resources univocally",
        compute="_compute_backend_url",
    )

    active = fields.Boolean(default=True,
                            help="If the active field is set to False, it will allow you to hide the process without removing it.")
    sequence = fields.Integer(
        default=10, help="Gives the sequence order when displaying a list of Processes.")
    partner_id = fields.Many2one('res.partner', string='Customer', auto_join=True, tracking=True,
                                 domain="['|', ('company_id', '=', False), ('company_id', '=', company_id)]")
    company_id = fields.Many2one(
        'res.company', string='Company', required=True, default=lambda self: self.env.company)

    favorite_user_ids = fields.Many2many(
        'res.users', 'process_favorite_user_rel', 'process_id', 'user_id',
        default=_get_default_favorite_user_ids,
        string='Members')
    label_tasks = fields.Char(string='Use Tasks as', default='Tasks', help="Label used for the tasks of the process.",
                              translate=True)
    tasks = fields.One2many('process.task', 'process_id',
                            string="Task Activities")
    resource_calendar_id = fields.Many2one(
        'resource.calendar', string='Working Time',
        default=lambda self: self.env.company.resource_calendar_id.id,
        domain="['|', ('company_id', '=', False), ('company_id', '=', company_id)]",
        help="Timetable working hours to adjust the gantt diagram report")
    type_ids = fields.Many2many('process.task.type', 'process_task_type_rel', 'process_id', 'type_id',
                                string='Tasks Stages')
    task_count = fields.Integer(
        compute='_compute_task_count', string="Task Count")
    task_ids = fields.One2many('process.task', 'process_id', string='Tasks',
                               domain=['|', ('stage_id.fold', '=', False), ('stage_id', '=', False)])
    color = fields.Integer(string='Color Index')
    user_id = fields.Many2one('res.users', string='Process Manager',
                              default=lambda self: self.env.user, tracking=True)
    alias_id = fields.Many2one('mail.alias', string='Alias', ondelete="restrict", required=True,
                               help="Internal email associated with this process. Incoming emails are automatically synchronized "
                                    "with Tasks (or optionally Issues if the Issue Tracker module is installed).")
    privacy_visibility = fields.Selection([
        ('followers', 'Invited employees'),
        ('employees', 'All employees'),
        ('portal', 'Portal users and all employees'),
    ],
        string='Visibility', required=True,
        default='portal',
        help="Defines the visibility of the tasks of the process:\n"
             "- Invited employees: employees may only see the followed process and tasks.\n"
             "- All employees: employees may see all process and tasks.\n"
             "- Portal users and all employees: employees may see everything."
             "   Portal users may see process and tasks followed by.\n"
             "   them or by someone of their company.")
    doc_count = fields.Integer(
        compute='_compute_attached_docs_count', string="Number of documents attached")
    date_start = fields.Date(string='Start Date')
    date = fields.Date(string='Expiration Date', index=True, tracking=True)
    subtask_process_id = fields.Many2one('process.process', string='Sub-task Process', ondelete="restrict",
                                         help="process in which sub-tasks of the current process will be created. It can be the current process itself.")

    _sql_constraints = [
        ('process_date_greater', 'check(date >= date_start)',
         'Error! process start-date must be lower than process end-date.')
    ]

    @api.depends("parent_id", "parent_id.number", "number")
    def compute_key(self):
        # for rec in self:
        if self.parent_id:
            temp = self.parent_id.number + '.' + \
                str(len(self.parent_id.child_ids) + 1)
            self.number = temp

    def _compute_access_url(self):
        super(Process, self)._compute_access_url()
        for process in self:
            process.access_url = '/my/process/%s' % process.id

    def _compute_access_warning(self):
        super(Process, self)._compute_access_warning()
        for process in self.filtered(lambda x: x.privacy_visibility != 'portal'):
            process.access_warning = _(
                "The process cannot be shared with the recipient(s) because the privacy of the process is too restricted. Set the privacy to 'Visible by following customers' in order to make it accessible by the recipient(s).")

    @api.model
    def _map_tasks_default_valeus(self, task, process):
        """ get the default value for the copied task on process duplication """
        return {
            'stage_id': task.stage_id.id,
            'name': task.name,
            'company_id': process.company_id.id,
        }

    def map_tasks(self, new_process_id):
        """ copy and map tasks from old to new process """
        process = self.browse(new_process_id)
        tasks = self.env['process.task']
        # We want to copy archived task, but do not propagate an active_test context key
        task_ids = self.env['process.task'].with_context(active_test=False).search([('process_id', '=', self.id)],
                                                                                   order='parent_id').ids
        old_to_new_tasks = {}
        for task in self.env['process.task'].browse(task_ids):
            # preserve task name and stage, normally altered during copy
            defaults = self._map_tasks_default_valeus(task, process)
            if task.parent_id:
                # set the parent to the duplicated task
                defaults['parent_id'] = old_to_new_tasks.get(
                    task.parent_id.id, False)
            new_task = task.copy(defaults)
            old_to_new_tasks[task.id] = new_task.id
            tasks += new_task

        return process.write({'tasks': [(6, 0, tasks.ids)]})

    @api.returns('self', lambda value: value.id)
    def copy(self, default=None):
        if default is None:
            default = {}
        if not default.get('name'):
            default['name'] = _("%s (copy)") % (self.name)
        process = super(Process, self).copy(default)
        if self.subtask_process_id == self:
            process.subtask_process_id = process
        for follower in self.message_follower_ids:
            process.message_subscribe(
                partner_ids=follower.partner_id.ids, subtype_ids=follower.subtype_ids.ids)
        if 'tasks' not in default:
            self.map_tasks(process.id)
        return process

    def write(self, vals):
        # print("INFORMATION:",vals)
        # if vals.get('child_process_provider_ids'):
        #    print("Child Process Provider:",vals['child_process_provider_ids'])
        #    array_of_ids = vals['child_process_provider_ids'][0][2]
        #    print(array_of_ids)
        res = super(Process, self).write(vals) if vals else True
        if 'active' in vals:
            # archiving/unarchiving a process does it on its tasks, too
            self.with_context(active_test=False).mapped(
                'tasks').write({'active': vals['active']})
        if vals.get('partner_id') or vals.get('privacy_visibility'):
            for process in self.filtered(lambda process: process.privacy_visibility == 'portal'):
                process.message_subscribe(process.partner_id.ids)
        return res

    def unlink(self):
        # Check process is empty
        for process in self.with_context(active_test=False):
            if process.tasks:
                raise UserError(_(
                    'You cannot delete a process containing tasks. You can either archive it or first delete all of its tasks.'))
            if process.child_ids:
                raise UserError(_(
                    'You cannot delete a process containing other processes. You can either archive it or first delete all of its tasks.'))
        result = super(Process, self).unlink()
        return result

    def message_subscribe(self, partner_ids=None, channel_ids=None, subtype_ids=None):
        """ Subscribe to all existing active tasks when subscribing to a process """
        res = super(Process, self).message_subscribe(partner_ids=partner_ids, channel_ids=channel_ids,
                                                     subtype_ids=subtype_ids)
        process_subtypes = self.env['mail.message.subtype'].browse(
            subtype_ids) if subtype_ids else None
        task_subtypes = (process_subtypes.mapped('parent_id') | process_subtypes.filtered(
            lambda sub: sub.internal or sub.default)).ids if process_subtypes else None
        if not subtype_ids or task_subtypes:
            self.mapped('tasks').message_subscribe(
                partner_ids=partner_ids, channel_ids=channel_ids, subtype_ids=task_subtypes)
        return res

    def message_unsubscribe(self, partner_ids=None, channel_ids=None):
        """ Unsubscribe from all tasks when unsubscribing from a process """
        self.mapped('tasks').message_unsubscribe(
            partner_ids=partner_ids, channel_ids=channel_ids)
        return super(Process, self).message_unsubscribe(partner_ids=partner_ids, channel_ids=channel_ids)

    # ---------------------------------------------------
    #  Actions
    # ---------------------------------------------------

    def open_tasks(self):
        # pdb.set_trace()
        ctx = dict(self._context)
        ctx.update({'search_default_process_id': self.id})
        print(ctx)
        action = self.env['ir.actions.act_window'].for_xml_id(
            'process', 'act_process_process_2_process_task_all')
        return dict(action, context=ctx)

    # ---------------------------------------------------
    #  History methods
    # ---------------------------------------------------

    @api.depends("menu_id", "parent_id.menu_id")
    def _compute_backend_url(self):
        tmpl = "/web#id={}&model=process.process&view_type=form"
        for rec in self:
            url = tmpl.format(rec.id)
            # retrieve action
            action = None
            parent = rec
            while not action and parent:
                action = parent.menu_id.action
                parent = parent.parent_id
            if action:
                url += "&action={}".format(action.id)
            rec.backend_url = url

    @api.constrains("parent_id")
    def _check_parent_id(self):
        if not self._check_recursion():
            raise ValidationError(_("You cannot create recursive categories."))

    def _get_page_index(self, link=True):
        """Return the index of a document."""
        self.ensure_one()
        index = [
            "<li>" + subpage._get_page_index() + "</li>" for subpage in self.child_ids
        ]
        r = ""
        if link:
            r = '<a href="{}">{}</a>'.format(self.backend_url, self.name)
        if index:
            r += "<ul>" + "".join(index) + "</ul>"
        return r

    @api.depends("history_head")
    def _compute_content(self):
        for rec in self:
            if rec.history_head:
                rec.content = rec.history_head.content
            else:
                # html widget's default, so it doesn't trigger ghost save
                rec.content = self._HTML_WIDGET_DEFAULT_VALUE

    def _inverse_content(self):
        for rec in self:
            if rec.content != rec.history_head.content:
                if rec.parent_id:
                    parent = rec.parent_id.number + '.' + rec.parent_id.name
                else:
                    parent = ""

                children = ""
                if rec.child_ids:
                    for x in rec.child_ids:
                        children = children + x.number + '.' + x.name + '\n'
                in_docs = ""
                out_docs = ""
                add_docs = ""
                if rec.in_document_ids:
                    for x in rec.in_document_ids:
                        in_docs = in_docs + x.name + '\n'

                if rec.out_document_ids:
                    for x in rec.out_document_ids:
                        out_docs = out_docs + x.name + '\n'

                if rec.add_document_ids:
                    for x in rec.add_document_ids:
                        add_docs = add_docs + x.name + '\n'
                rec._create_history(
                    {
                        "page_id": rec.id,
                        "name": rec.draft_name,
                        "summary": rec.draft_summary,
                        "content": rec.content,
                        "goal": rec.goal,
                        "strategic_kpi": rec.strategic_kpi,
                        "parent": parent,
                        "children": children,
                        "proc_name": rec.number + '.' + rec.name,
                        "in_docs": in_docs,
                        "out_docs": out_docs,
                        "add_docs": add_docs,
                    }
                )

    def _create_history(self, vals):
        self.ensure_one()
        return self.env["process.history"].create(vals)

    def _search_content(self, operator, value):
        return [("history_head.content", operator, value)]

    @api.depends("history_head")
    def _compute_name(self):
        for rec in self:
            if rec.history_head:
                rec.draft_name = rec.history_head.name
            else:
                # html widget's default, so it doesn't trigger ghost save
                rec.draft_name = ""

    def _inverse_name(self):
        for rec in self:
            if rec.draft_name != rec.history_head.name:
                if rec.parent_id:
                    parent = rec.parent_id.number + '.' + rec.parent_id.name
                else:
                    parent = ""

                children = ""
                if rec.child_ids:
                    for x in rec.child_ids:
                        children = children + x.number + '.' + x.name + '\n'
                in_docs = ""
                out_docs = ""
                add_docs = ""
                if rec.in_document_ids:
                    for x in rec.in_document_ids:
                        in_docs = in_docs + x.name + '\n'

                if rec.out_document_ids:
                    for x in rec.out_document_ids:
                        out_docs = out_docs + x.name + '\n'

                if rec.add_document_ids:
                    for x in rec.add_document_ids:
                        add_docs = add_docs + x.name + '\n'
                rec._create_history(
                    {
                        "page_id": rec.id,
                        "name": rec.draft_name,
                        "summary": rec.draft_summary,
                        "content": rec.content,
                        "goal": rec.goal,
                        "strategic_kpi": rec.strategic_kpi,
                        "parent": parent,
                        "children": children,
                        "proc_name": rec.number + '.' + rec.name,
                        "in_docs": in_docs,
                        "out_docs": out_docs,
                        "add_docs": add_docs,
                    }
                )

    def _search_name(self, operator, value):
        return [("history_head.name", operator, value)]

    def clear_draft(self):
        self.draft_name = ""

    @api.depends("history_ids")
    def _compute_history_head(self):
        for rec in self:
            if rec.history_ids:
                rec.history_head = rec.history_ids[0]
            else:
                rec.history_head = False

    # Доделать

    @api.model
    def create(self, vals):
        print('Значения(добавления):', vals.get('number'))
        # 4.4.9
        #
        # len_of_numbers = re.findall(vals.get('number'))
        # for i in array_of_numbers:
        #     instance = self.env['process.process'].create({
        #         'number':'Test',
        #         'name':'Test',
        #         'max_depth':i,
        #     })
        return super().create(vals)

    def write(self,vals):
        print('Значения(обновления):',vals)
        pprint.pprint(vals)
        if vals.get('process_lines_ids'):
            for i in vals.get('process_lines_ids'):
                provider = self.env['process.process.line'].browse(i[1])
                for j in i:
                    if j == 2:
                        print("=>Удалился")
                        print("объект:",self)
                        for rec in self:
                            
                            for lines in rec.in_process_lines_document_ids:
                                if lines.second_out_process_id == provider.provider_id:
                                    lines.unlink()

                    if j == 4:
                        print("=>Добавился")
        return super().write(vals)

    # @api.onchange('number')
    # def print_odd_number(self):
    #     print("NUMBER:",self.number)
    #     g = gv.Graph('G', engine='sfdp')
    #     if self.number:
    #         number_array = self.number.split('.')
    #         g.edge(number_array[0] + '.' + number_array[1],number_array[0])
    #         g.edge(number_array[0] + '.' + number_array[1] + number_array[2],number_array[0] + '.' + number_array[1])
    #         base64_png = g.pipe(format='png')
    #         converted_image = base64.b64encode(base64_png).decode('utf-8')
    #         print(converted_image)
    #         self.test_image = converted_image
    # def unlink(self,vals):
    #     print('Значения(удаления):',vals)
    #     return super().unlink(vals)
# ---------------------------------------------------
#  Business Methods
# ---------------------------------------------------

# ---------------------------------------------------
# Rating business
# ---------------------------------------------------


class Task(models.Model):
    _name = "process.task"
    _description = "Task"
    _date_name = "date_assign"
    _inherit = ['portal.mixin', 'mail.thread.cc',
                'mail.activity.mixin', 'rating.mixin']
    _mail_post_access = 'read'
    _order = "priority desc, sequence, id desc"
    _check_company_auto = True

    @api.model
    def default_get(self, fields_list):
        result = super(Task, self).default_get(fields_list)
        # find default value from parent for the not given ones
        parent_task_id = result.get(
            'parent_id') or self._context.get('default_parent_id')
        if parent_task_id:
            parent_values = self._subtask_values_from_parent(parent_task_id)
            for fname, value in parent_values.items():
                if fname not in result:
                    result[fname] = value
        return result

    @api.model
    def _get_default_partner(self):
        if 'default_process_id' in self.env.context:
            default_process_id = self.env['process.process'].browse(
                self.env.context['default_process_id'])
            return default_process_id.exists().partner_id

    def _get_default_stage_id(self):
        """ Gives default stage_id """
        process_id = self.env.context.get('default_process_id')
        if not process_id:
            return False
        return self.stage_find(process_id, [('fold', '=', False)])

    @api.model
    def _default_company_id(self):
        if self._context.get('default_process_id'):
            return self.env['process.process'].browse(self._context['default_process_id']).company_id
        return self.env.company

    @api.model
    def _read_group_stage_ids(self, stages, domain, order):
        search_domain = [('id', 'in', stages.ids)]
        if 'default_process_id' in self.env.context:
            search_domain = [
                '|', ('process_ids', '=', self.env.context['default_process_id'])] + search_domain

        stage_ids = stages._search(
            search_domain, order=order, access_rights_uid=SUPERUSER_ID)
        return stages.browse(stage_ids)

    active = fields.Boolean(default=True)
    name = fields.Char(string='Title', tracking=True,
                       required=True, index=True)
    description = fields.Html(string='Description')
    priority = fields.Selection([
        ('0', 'Normal'),
        ('1', 'Important'),
    ], default='0', index=True, string="Priority")
    sequence = fields.Integer(string='Sequence', index=True, default=10,
                              help="Gives the sequence order when displaying a list of tasks.")
    stage_id = fields.Many2one('process.task.type', string='Stage', ondelete='restrict', tracking=True, index=True,
                               default=_get_default_stage_id, group_expand='_read_group_stage_ids',
                               domain="[('process_ids', '=', process_id)]", copy=False)
    tag_ids = fields.Many2many('process.tags', string='Tags')
    kanban_state = fields.Selection([
        ('normal', 'Grey'),
        ('done', 'Green'),
        ('blocked', 'Red')], string='Kanban State',
        copy=False, default='normal', required=True)
    kanban_state_label = fields.Char(
        compute='_compute_kanban_state_label', string='Kanban State Label', tracking=True)
    create_date = fields.Datetime("Created On", readonly=True, index=True)
    write_date = fields.Datetime("Last Updated On", readonly=True, index=True)
    date_end = fields.Datetime(string='Ending Date', index=True, copy=False)
    date_assign = fields.Datetime(
        string='Assigning Date', index=True, copy=False, readonly=True)
    date_deadline = fields.Date(
        string='Deadline', index=True, copy=False, tracking=True)
    date_deadline_formatted = fields.Char(
        compute='_compute_date_deadline_formatted')
    date_last_stage_update = fields.Datetime(string='Last Stage Update',
                                             index=True,
                                             copy=False,
                                             readonly=True)
    process_id = fields.Many2one('process.process', string='Process',
                                 default=lambda self: self.env.context.get(
                                     'default_process_id'),
                                 index=True, tracking=True, check_company=True, change_default=True)
    planned_hours = fields.Float("Planned Hours",
                                 help='It is the time planned to achieve the task. If this document has sub-tasks, it means the time needed to achieve this tasks and its childs.',
                                 tracking=True)
    subtask_planned_hours = fields.Float("Subtasks", compute='_compute_subtask_planned_hours',
                                         help="Computed using sum of hours planned of all subtasks created from main task. Usually these hours are less or equal to the Planned Hours (of main task).")
    user_id = fields.Many2one('res.users',
                              string='Assigned to',
                              default=lambda self: self.env.uid,
                              index=True, tracking=True)
    partner_id = fields.Many2one('res.partner',
                                 string='Customer',
                                 default=lambda self: self._get_default_partner(),
                                 domain="['|', ('company_id', '=', False), ('company_id', '=', company_id)]")
    partner_city = fields.Char(related='partner_id.city', readonly=False)
    manager_id = fields.Many2one('res.users', string='Process Manager', related='process_id.user_id', readonly=True,
                                 related_sudo=False)
    company_id = fields.Many2one(
        'res.company', string='Company', required=True, default=_default_company_id)
    color = fields.Integer(string='Color Index')
    user_email = fields.Char(
        related='user_id.email', string='User Email', readonly=True, related_sudo=False)
    attachment_ids = fields.One2many('ir.attachment', compute='_compute_attachment_ids', string="Main Attachments",
                                     help="Attachment that don't come from message.")
    # In the domain of displayed_image_id, we couln't use attachment_ids because a one2many is represented as a list of commands so we used res_model & res_id
    displayed_image_id = fields.Many2one('ir.attachment',
                                         domain="[('res_model', '=', 'process.task'), ('res_id', '=', id), ('mimetype', 'ilike', 'image')]",
                                         string='Cover Image')
    legend_blocked = fields.Char(related='stage_id.legend_blocked', string='Kanban Blocked Explanation', readonly=True,
                                 related_sudo=False)
    legend_done = fields.Char(related='stage_id.legend_done', string='Kanban Valid Explanation', readonly=True,
                              related_sudo=False)
    legend_normal = fields.Char(related='stage_id.legend_normal', string='Kanban Ongoing Explanation', readonly=True,
                                related_sudo=False)
    parent_id = fields.Many2one(
        'process.task', string='Parent Task', index=True)
    child_ids = fields.One2many(
        'process.task', 'parent_id', string="Sub-tasks", context={'active_test': False})
    subtask_process_id = fields.Many2one('process.process', related="process_id.subtask_process_id",
                                         string='Sub-task Process', readonly=True)
    subtask_count = fields.Integer(
        "Sub-task count", compute='_compute_subtask_count')
    email_from = fields.Char(
        string='Email', help="These people will receive email.", index=True)
    # Computed field about working time elapsed between record creation and assignation/closing.
    working_hours_open = fields.Float(compute='_compute_elapsed', string='Working hours to assign', store=True,
                                      group_operator="avg")
    working_hours_close = fields.Float(compute='_compute_elapsed', string='Working hours to close', store=True,
                                       group_operator="avg")
    working_days_open = fields.Float(compute='_compute_elapsed', string='Working days to assign', store=True,
                                     group_operator="avg")
    working_days_close = fields.Float(compute='_compute_elapsed', string='Working days to close', store=True,
                                      group_operator="avg")
    # customer portal: include comment and incoming emails in communication history
    website_message_ids = fields.One2many(
        domain=lambda self: [('model', '=', self._name), ('message_type', 'in', ['email', 'comment'])])

    @api.depends('date_deadline')
    def _compute_date_deadline_formatted(self):
        for task in self:
            task.date_deadline_formatted = format_date(
                self.env, task.date_deadline) if task.date_deadline else None

    def _compute_attachment_ids(self):
        for task in self:
            attachment_ids = self.env['ir.attachment'].search(
                [('res_id', '=', task.id), ('res_model', '=', 'process.task')]).ids
            message_attachment_ids = task.mapped(
                'message_ids.attachment_ids').ids  # from mail_thread
            task.attachment_ids = [
                (6, 0, list(set(attachment_ids) - set(message_attachment_ids)))]

    @api.depends('create_date', 'date_end', 'date_assign')
    def _compute_elapsed(self):
        task_linked_to_calendar = self.filtered(
            lambda task: task.process_id.resource_calendar_id and task.create_date
        )
        for task in task_linked_to_calendar:
            dt_create_date = fields.Datetime.from_string(task.create_date)

            if task.date_assign:
                dt_date_assign = fields.Datetime.from_string(task.date_assign)
                duration_data = task.process_id.resource_calendar_id.get_work_duration_data(dt_create_date,
                                                                                            dt_date_assign,
                                                                                            compute_leaves=True)
                task.working_hours_open = duration_data['hours']
                task.working_days_open = duration_data['days']
            else:
                task.working_hours_open = 0.0
                task.working_days_open = 0.0

            if task.date_end:
                dt_date_end = fields.Datetime.from_string(task.date_end)
                duration_data = task.process_id.resource_calendar_id.get_work_duration_data(dt_create_date, dt_date_end,
                                                                                            compute_leaves=True)
                task.working_hours_close = duration_data['hours']
                task.working_days_close = duration_data['days']
            else:
                task.working_hours_close = 0.0
                task.working_days_close = 0.0

        (self - task_linked_to_calendar).update(dict.fromkeys(
            ['working_hours_open', 'working_hours_close', 'working_days_open', 'working_days_close'], 0.0))

    @api.depends('stage_id', 'kanban_state')
    def _compute_kanban_state_label(self):
        for task in self:
            if task.kanban_state == 'normal':
                task.kanban_state_label = task.legend_normal
            elif task.kanban_state == 'blocked':
                task.kanban_state_label = task.legend_blocked
            else:
                task.kanban_state_label = task.legend_done

    def _compute_access_url(self):
        super(Task, self)._compute_access_url()
        for task in self:
            task.access_url = '/my/task/%s' % task.id

    def _compute_access_warning(self):
        super(Task, self)._compute_access_warning()
        for task in self.filtered(lambda x: x.process_id.privacy_visibility != 'portal'):
            task.access_warning = _(
                "The task cannot be shared with the recipient(s) because the privacy of the process is too restricted. Set the privacy of the process to 'Visible by following customers' in order to make it accessible by the recipient(s).")

    @api.depends('child_ids.planned_hours')
    def _compute_subtask_planned_hours(self):
        for task in self:
            task.subtask_planned_hours = sum(
                task.child_ids.mapped('planned_hours'))

    @api.depends('child_ids')
    def _compute_subtask_count(self):
        """ Note: since we accept only one level subtask, we can use a read_group here """
        task_data = self.env['process.task'].read_group(
            [('parent_id', 'in', self.ids)], ['parent_id'], ['parent_id'])
        mapping = dict((data['parent_id'][0], data['parent_id_count'])
                       for data in task_data)
        for task in self:
            task.subtask_count = mapping.get(task.id, 0)

    @api.onchange('partner_id')
    def _onchange_partner_id(self):
        self.email_from = self.partner_id.email

    @api.onchange('parent_id')
    def _onchange_parent_id(self):
        if self.parent_id:
            for field_name, value in self._subtask_values_from_parent(self.parent_id.id).items():
                if not self[field_name]:
                    self[field_name] = value

    @api.onchange('process_id')
    def _onchange_process(self):
        if self.process_id:
            # find partner
            if self.process_id.partner_id:
                self.partner_id = self.process_id.partner_id
            # find stage
            if self.process_id not in self.stage_id.process_ids:
                self.stage_id = self.stage_find(
                    self.process_id.id, [('fold', '=', False)])
            # keep multi company consistency
            self.company_id = self.process_id.company_id
        else:
            self.stage_id = False

    @api.onchange('company_id')
    def _onchange_task_company(self):
        if self.process_id.company_id != self.company_id:
            self.process_id = False

    @api.constrains('parent_id', 'child_ids')
    def _check_subtask_level(self):
        for task in self:
            if task.parent_id and task.child_ids:
                raise ValidationError(
                    _('Task %s cannot have several subtask levels.' % (task.name,)))

    @api.returns('self', lambda value: value.id)
    def copy(self, default=None):
        if default is None:
            default = {}
        if not default.get('name'):
            default['name'] = _("%s (copy)") % self.name
        return super(Task, self).copy(default)

    @api.constrains('parent_id')
    def _check_parent_id(self):
        for task in self:
            if not task._check_recursion():
                raise ValidationError(
                    _('Error! You cannot create recursive hierarchy of task(s).'))

    @api.model
    def get_empty_list_help(self, help):
        tname = _("task")
        process_id = self.env.context.get('default_process_id', False)
        if process_id:
            name = self.env['process.process'].browse(process_id).label_tasks
            if name:
                tname = name.lower()

        self = self.with_context(
            empty_list_help_id=self.env.context.get('default_process_id'),
            empty_list_help_model='process.process',
            empty_list_help_document_name=tname,
        )
        return super(Task, self).get_empty_list_help(help)

    # ----------------------------------------
    # Case management
    # ----------------------------------------

    def stage_find(self, section_id, domain=[], order='sequence'):
        """ Override of the base.stage method
            Parameter of the stage search taken from the lead:
            - section_id: if set, stages must belong to this section or
              be a default stage; if not set, stages must be default
              stages
        """
        # collect all section_ids
        section_ids = []
        if section_id:
            section_ids.append(section_id)
        section_ids.extend(self.mapped('process_id').ids)
        search_domain = []
        if section_ids:
            search_domain = [('|')] * (len(section_ids) - 1)
            for section_id in section_ids:
                search_domain.append(('process_ids', '=', section_id))
        search_domain += list(domain)
        # perform search, return the first found
        return self.env['process.task.type'].search(search_domain, order=order, limit=1).id

    # ------------------------------------------------
    # CRUD overrides
    # ------------------------------------------------

    @api.model
    def create(self, vals):
        # context: no_log, because subtype already handle this
        print("VALS:", vals)
        context = dict(self.env.context)
        print("CONTEXT:", context)
        # for default stage
        if vals.get('process_id') and not context.get('default_process_id'):
            context['default_process_id'] = vals.get('process_id')
        # user_id change: update date_assign
        if vals.get('user_id'):
            vals['date_assign'] = fields.Datetime.now()
        # Stage change: Update date_end if folded stage and date_last_stage_update
        if vals.get('stage_id'):
            vals.update(self.update_date_end(vals['stage_id']))
            vals['date_last_stage_update'] = fields.Datetime.now()
        # substask default values
        if vals.get('parent_id'):
            for fname, value in self._subtask_values_from_parent(vals['parent_id']).items():
                if fname not in vals:
                    vals[fname] = value
        task = super(Task, self.with_context(context)).create(vals)
        if task.process_id.privacy_visibility == 'portal':
            task._portal_ensure_token()
        return task

    def write(self, vals):
        now = fields.Datetime.now()
        # stage change: update date_last_stage_update
        if 'stage_id' in vals:
            vals.update(self.update_date_end(vals['stage_id']))
            vals['date_last_stage_update'] = now
            # reset kanban state when changing stage
            if 'kanban_state' not in vals:
                vals['kanban_state'] = 'normal'
        # user_id change: update date_assign
        if vals.get('user_id') and 'date_assign' not in vals:
            vals['date_assign'] = now
        result = super(Task, self).write(vals)
        # rating on stage
        if 'stage_id' in vals and vals.get('stage_id'):
            # self.filtered(lambda x: x.process_id.rating_status == 'stage')._send_task_rating_mail(force_send=True)
            self._send_task_rating_mail(force_send=True)
        return result

    def update_date_end(self, stage_id):
        process_task_type = self.env['process.task.type'].browse(stage_id)
        if process_task_type.fold:
            return {'date_end': fields.Datetime.now()}
        return {'date_end': False}

    # ---------------------------------------------------
    # Subtasks
    # ---------------------------------------------------

    def _subtask_default_fields(self):
        """ Return the list of field name for default value when creating a subtask """
        return ['partner_id', 'email_from']

    def _subtask_values_from_parent(self, parent_id):
        """ Get values for substask implied field of the given"""
        result = {}
        parent_task = self.env['process.task'].browse(parent_id)
        for field_name in self._subtask_default_fields():
            result[field_name] = parent_task[field_name]
        # special case for the subtask default process
        result['process_id'] = parent_task.process_id.subtask_process_id
        return self._convert_to_write(result)

    # ---------------------------------------------------
    # Mail gateway
    # ---------------------------------------------------

    def _track_template(self, changes):
        res = super(Task, self)._track_template(changes)
        test_task = self[0]
        if 'stage_id' in changes and test_task.stage_id.mail_template_id:
            res['stage_id'] = (test_task.stage_id.mail_template_id, {
                'auto_delete_message': True,
                'subtype_id': self.env['ir.model.data'].xmlid_to_res_id('mail.mt_note'),
                'email_layout_xmlid': 'mail.mail_notification_light'
            })
        return res

    def _creation_subtype(self):
        return self.env.ref('process.mt_task_new')

    def _track_subtype(self, init_values):
        self.ensure_one()
        if 'kanban_state_label' in init_values and self.kanban_state == 'blocked':
            return self.env.ref('process.mt_task_blocked')
        elif 'kanban_state_label' in init_values and self.kanban_state == 'done':
            return self.env.ref('process.mt_task_ready')
        elif 'stage_id' in init_values:
            return self.env.ref('process.mt_task_stage')
        return super(Task, self)._track_subtype(init_values)

    def _notify_get_groups(self, msg_vals=None):
        """ Handle process users and managers recipients that can assign
        tasks and create new one directly from notification emails. Also give
        access button to portal users and portal customers. If they are notified
        they should probably have access to the document. """
        groups = super(Task, self)._notify_get_groups(msg_vals=msg_vals)
        msg_vals = msg_vals or {}
        self.ensure_one()

        process_user_group_id = self.env.ref('process.group_process_user').id
        new_group = (
            'group_process_user',
            lambda pdata: pdata['type'] == 'user' and process_user_group_id in pdata['groups'],
            {},
        )

        if not self.user_id and not self.stage_id.fold:
            take_action = self._notify_get_action_link('assign', **msg_vals)
            process_actions = [{'url': take_action, 'title': _('I take it')}]
            new_group[2]['actions'] = process_actions

        groups = [new_group] + groups

        for group_name, group_method, group_data in groups:
            if group_name != 'customer':
                group_data['has_button_access'] = True

        return groups

    def _notify_get_reply_to(self, default=None, records=None, company=None, doc_names=None):
        """ Override to set alias of tasks to their process if any. """
        aliases = self.sudo().mapped('process_id')._notify_get_reply_to(default=default, records=None, company=company,
                                                                        doc_names=None)
        res = {task.id: aliases.get(task.process_id.id) for task in self}
        leftover = self.filtered(lambda rec: not rec.process_id)
        if leftover:
            res.update(super(Task, leftover)._notify_get_reply_to(default=default, records=None, company=company,
                                                                  doc_names=doc_names))
        return res

    def email_split(self, msg):
        email_list = tools.email_split(
            (msg.get('to') or '') + ',' + (msg.get('cc') or ''))
        # check left-part is not already an alias
        aliases = self.mapped('process_id.alias_name')
        return [x for x in email_list if x.split('@')[0] not in aliases]

    @api.model
    def message_new(self, msg, custom_values=None):
        """ Overrides mail_thread message_new that is called by the mailgateway
            through message_process.
            This override updates the document according to the email.
        """
        # remove default author when going through the mail gateway. Indeed we
        # do not want to explicitly set user_id to False; however we do not
        # want the gateway user to be responsible if no other responsible is
        # found.
        create_context = dict(self.env.context or {})
        create_context['default_user_id'] = False
        if custom_values is None:
            custom_values = {}
        defaults = {
            'name': msg.get('subject') or _("No Subject"),
            'email_from': msg.get('from'),
            'planned_hours': 0.0,
            'partner_id': msg.get('author_id')
        }
        defaults.update(custom_values)

        task = super(Task, self.with_context(create_context)
                     ).message_new(msg, custom_values=defaults)
        email_list = task.email_split(msg)
        partner_ids = [p.id for p in self.env['mail.thread']._mail_find_partner_from_emails(email_list, records=task,
                                                                                            force_create=False) if p]
        task.message_subscribe(partner_ids)
        return task

    def message_update(self, msg, update_vals=None):
        """ Override to update the task according to the email. """
        email_list = self.email_split(msg)
        partner_ids = [p.id for p in self.env['mail.thread']._mail_find_partner_from_emails(email_list, records=self,
                                                                                            force_create=False) if p]
        self.message_subscribe(partner_ids)
        return super(Task, self).message_update(msg, update_vals=update_vals)

    def _message_get_suggested_recipients(self):
        recipients = super(Task, self)._message_get_suggested_recipients()
        for task in self:
            if task.partner_id:
                reason = _('Customer Email') if task.partner_id.email else _(
                    'Customer')
                task._message_add_suggested_recipient(
                    recipients, partner=task.partner_id, reason=reason)
            elif task.email_from:
                task._message_add_suggested_recipient(
                    recipients, email=task.email_from, reason=_('Customer Email'))
        return recipients

    def _notify_email_header_dict(self):
        headers = super(Task, self)._notify_email_header_dict()
        if self.process_id:
            current_objects = [h for h in headers.get(
                'X-Odoo-Objects', '').split(',') if h]
            current_objects.insert(
                0, 'process.process-%s, ' % self.process_id.id)
            headers['X-Odoo-Objects'] = ','.join(current_objects)
        if self.tag_ids:
            headers['X-Odoo-Tags'] = ','.join(self.tag_ids.mapped('name'))
        return headers

    def _message_post_after_hook(self, message, msg_vals):
        if self.email_from and not self.partner_id:
            # we consider that posting a message with a specified recipient (not a follower, a specific one)
            # on a document without customer means that it was created through the chatter using
            # suggested recipients. This heuristic allows to avoid ugly hacks in JS.
            new_partner = message.partner_ids.filtered(
                lambda partner: partner.email == self.email_from)
            if new_partner:
                self.search([
                    ('partner_id', '=', False),
                    ('email_from', '=', new_partner.email),
                    ('stage_id.fold', '=', False)]).write({'partner_id': new_partner.id})
        return super(Task, self)._message_post_after_hook(message, msg_vals)

    def action_assign_to_me(self):
        self.write({'user_id': self.env.user.id})

    def action_open_parent_task(self):
        return {
            'name': _('Parent Task'),
            'view_mode': 'form',
            'res_model': 'process.task',
            'res_id': self.parent_id.id,
            'type': 'ir.actions.act_window',
            'context': dict(self._context, create=False)
        }

    def action_subtask(self):
        action = self.env.ref('process.process_task_action_sub_task').read()[0]

        # only display subtasks of current task
        action['domain'] = [('id', 'child_of', self.id), ('id', '!=', self.id)]

        # update context, with all default values as 'quick_create' does not contains all field in its view
        if self._context.get('default_process_id'):
            default_process = self.env['process.process'].browse(
                self.env.context['default_process_id'])
        else:
            default_process = self.process_id.subtask_process_id or self.process_id
        ctx = dict(self.env.context)
        ctx.update({
            'default_name': self.env.context.get('name', self.name) + ':',
            'default_parent_id': self.id,  # will give default subtask field in `default_get`
            'default_company_id': default_process.company_id.id if default_process else self.env.company.id,
            'search_default_parent_id': self.id,
        })
        parent_values = self._subtask_values_from_parent(self.id)
        for fname, value in parent_values.items():
            if 'default_' + fname not in ctx:
                ctx['default_' + fname] = value
        action['context'] = ctx

        return action

    # ---------------------------------------------------
    # Rating business
    # ---------------------------------------------------

    def _send_task_rating_mail(self, force_send=False):
        for task in self:
            rating_template = task.stage_id.rating_template_id
            if rating_template:
                task.rating_send_request(
                    rating_template, lang=task.partner_id.lang, force_send=force_send)

    def rating_get_partner_id(self):
        res = super(Task, self).rating_get_partner_id()
        if not res and self.process_id.partner_id:
            return self.process_id.partner_id
        return res

    def rating_apply(self, rate, token=None, feedback=None, subtype=None):
        return super(Task, self).rating_apply(rate, token=token, feedback=feedback, subtype="process.mt_task_rating")

    def _rating_get_parent_field_name(self):
        return 'process_id'


class ProcessTags(models.Model):
    """ Tags of process's tasks """
    _name = "process.tags"
    _description = "Process Tags"

    name = fields.Char('Tag Name', required=True)
    color = fields.Integer(string='Color Index')

    _sql_constraints = [
        ('name_uniq', 'unique (name)', "Tag name already exists!"),
    ]


# class ProcessEvent(models.Model):
#     _name = 'process.event'
#     _description = 'Process Event'

#     name = fields.Char('Имя ивента',required = True)
#     color = fields.Integer(string='Цвет')
#     process_id = fields.Many2one('process.process',string='Процессы')


# class InheritDocumentPage(models.Model):
#     _inherit = 'document.page'


#     stage = fields.Selection(
#         [
#             ('draft','Draft'),
#             ('signed','Signed')
#         ],string='Stage of Documents',default='draft'
#     )
#     future_stage = fields.Selection(
#         [
#             ('draft','Draft'),
#             ('signed','Signed')
#         ], string='Future Stage of Documents'
#     )

#     main_document = fields.Boolean(string='Chief document')

#     def button_change_to_signed(self):
#         if self.stage == 'draft':
#             self.stage = 'signed'
#         else:
#             raise ValidationError('Could not change to draft!')
